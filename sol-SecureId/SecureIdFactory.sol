pragma solidity ^0.4.15;

import './SecureId.sol';

contract SecureIdFactory {
    mapping(address => address[]) public personalSecureIds;
    mapping(address => uint) public secureIdPointers;
    mapping(address => address) public secureIdOwners;
    mapping(address => string) public secureIdDetail;
    mapping(address => string) public secureIdDistribution;
    mapping(address => address) public lastSecureId;

    event GeneratedSecureId(address senderId, address _sId, uint256 timestamp);
    event UpdatedSecureId(address senderId, address _sId, uint256 timestamp);
    
    function generateSecureId (string _detailsJson) {

        //address sId = new SecureId(_detailsJson,_distributionsJson);
        address sId = new SecureId();

        personalSecureIds[msg.sender].push(sId);
        secureIdPointers[sId] = personalSecureIds[msg.sender].length - 1;
        secureIdOwners[sId] = msg.sender;
        secureIdDetail[sId] = _detailsJson;
        //secureIdDistribution[sId] = _distributionsJson;
        lastSecureId[msg.sender] = sId;

        GeneratedSecureId(msg.sender,sId,block.timestamp);
    }

    function updateSecureId (address _secureId, string _detailsJson) {
        secureIdDetail[_secureId] = _detailsJson;

        UpdatedSecureId(msg.sender,_secureId,block.timestamp);
    }

    function getLastSecureId(address from) public constant returns (address) {
        return lastSecureId[from];
    }

    function getSecureIdInfoByOwner(address _secureIdOwner, uint _i) constant returns (string) {
        SecureId sId = SecureId(personalSecureIds[_secureIdOwner][_i]);
        return (secureIdDetail[sId]);
    }

    function getSecureIdDistributionByOwner(address _secureIdOwner, uint _i) constant returns (string) {
        SecureId sId = SecureId(personalSecureIds[_secureIdOwner][_i]);
        return (secureIdDistribution[sId]);
    }

    function getSecureIdCounter(address _secureIdOwner, uint _i) constant returns (uint) {
        SecureId sId = SecureId(personalSecureIds[_secureIdOwner][_i]);
        
        return (sId.counter());
    }

    function getSecureIdInfo (address _secureId) constant returns (string) {
        address secureIdOwner = secureIdOwners[_secureId];
        uint contractIndex = secureIdPointers[_secureId];
        
        return getSecureIdInfoByOwner(secureIdOwner,contractIndex);
    }
    
    function getSecureIdDistribution (address _secureId) constant returns (string) {
        address secureIdOwner = secureIdOwners[_secureId];
        uint contractIndex = secureIdPointers[_secureId];
        
        return getSecureIdDistributionByOwner(secureIdOwner,contractIndex);
    }

    // function get10SecureIdInfosByOwner (address _secureIdOwner,uint _i) constant returns (string[10]) {
    //     uint length = personalSecureIds[_secureIdOwner].length;
    //     require(_i < length);
        
    //     string[10] memory result;
    //     uint position = 0;
    //     uint pivot = _i;
    //     SecureId sId;

    //     if(_i + 10 < length) {
    //         for(position = 0; position < 10; position++) {
    //             sId = SecureId(personalSecureIds[_secureIdOwner][pivot]);
    //             result[position] = secureIdDetail[sId];
    //             pivot++;
    //         }
    //     } else if(_i + 10 >= length) {
    //         for(position = 0; position < length - _i; position++) {
    //             sId = SecureId(personalSecureIds[_secureIdOwner][pivot]);
    //             result[position] = secureIdDetail[sId];
    //             pivot++;
    //         }
    //     }

    //     return result;
    // }

    // function getSecureIdLocations(uint _i,uint j) constant returns (uint,bytes32,bytes32,bytes32) {
    //     SecureId sId = SecureId(secureStickers[_i]);

    //     var (a,b,c,d) = sId.getLocation(j);

    //     return (a,b,c,d);
    // }

    // function get10Contracts(address contractOwner,uint _index) constant returns (address[10]) {
    //     address[10] memory result;

    //     if(index > personalStickers[contractOwner].length) {
    //         return result;
    //     }

    //     if(index + 10 > personalStickers[contractOwner].length) {
    //         for(index; index < personalStickers[contractOwner].length; index ++) {
    //             result[index] = personalStickers[contractOwner][index];
    //         }
    //     } else {
    //         for(index; index < personalStickers[contractOwner].length; index++) {
    //             result[index] = personalStickers[contractOwner][index];
    //         }
    //     }

    //     return result;
    // }

    function setDummy() {
        address sId = new SecureId();
        //secureStickers.push(sId);
        personalSecureIds[msg.sender].push(sId);
        secureIdPointers[sId] = personalSecureIds[msg.sender].length - 1;
        secureIdOwners[sId] = msg.sender;
    }
}