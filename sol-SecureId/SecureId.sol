pragma solidity ^0.4.15;

contract SecureId {
    string public detailsJson;
    string public distributionsJson;
    uint public counter;

    function SecureId () {
        counter = 0;
    }

    // function SecureId (string _detailsJson, string _distributionsJson) {
    //     // batchId = _batchId;
    //     // sniCode = _sniCode;
    //     // productName = _productName;
    //     // productionDate = _productionDate;
    //     // expirationDate = _expirationDate;
    //     // amount = _amount;
    //     // status = 0;
    //     detailsJson = _detailsJson;
    //     distributionsJson = _distributionsJson;
    //     counter = 0;

    // }

    function getDetails() constant returns (string) {
        return detailsJson;
    }
    
    function getDistributions() constant returns (string) {
        return distributionsJson;
    }

    function updateSecureId(string _detailsJson) {
        detailsJson = _detailsJson;
        counter++;
    }

    function updateDistributions(string _distributionsJson) {
        distributionsJson = _distributionsJson;
    }

    function addCounter() returns (uint) {
        counter += 1;

        return counter;
    }
    // struct DistributionLocation {
    //     uint date;
    //     bytes32 name;
    //     bytes32 distributiontype;
    //     bytes32 city;
    // }
}