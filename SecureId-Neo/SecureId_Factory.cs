﻿using Neo.SmartContract.Framework;
using Neo.SmartContract.Framework.Services.Neo;
using System.Collections.Generic;
//using Neo.SmartContract.Framework.Services.System;
//using System;
//using System.Numerics;
//using System.Collections;

using static SecureId_Neo.SecureId;

namespace SecureId_Neo
{
    public class SecureId_factory : SmartContract
    {
        private static Dictionary<Account, Account[]> personalSecureIds = new Dictionary<Account, Account[]>();
        private static Dictionary<Account, uint> secureIdPointers = new Dictionary<Account, uint>();
        private static Dictionary<Account, Account> secureIdOwners = new Dictionary<Account, Account>();
        private static Dictionary<Account, string> secureIdDetail = new Dictionary<Account, string>();
        private static Dictionary<Account, string> secureIdDistribution = new Dictionary<Account, string>();
        private static Dictionary<Account, Account> lastSecureId = new Dictionary<Account, Account>();

        //private static StorageContext storage = new StorageContext();

        //Function main penting pada Neo
        //Input: SecureId_Factory(<method>, args));
        public static void Main(string method, params object[] args)
        {
            //Storage.Put(storage, "personalSecureIds", personalSecureIds);
            // Method storage for store data
            byte[] secureId;

            switch (method)
            {
                case "generateSecureId":
                    //Input: args[0] = (string)detailJson
                    secureId = generateSecureId((string)args[0]);
                    break;
                case "updateSecureId":
                    updateSecureId(
                        Blockchain.GetAccount((byte[])args[0]),
                        (string)args[1]
                        );
                    break;
                case "getLastSecureId":
                    getLastSecureId(Blockchain.GetAccount((byte[])args[0]));
                    break;
                case "getSecureIdInfoByOwner":
                    getSecureIdInfoByOwner(
                        Blockchain.GetAccount((byte[])args[0]),
                        (uint)args[1]
                        );
                    break;
                case "getSecureIdDistributionByOwner":
                    getSecureIdDistributionByOwner(
                        Blockchain.GetAccount((byte[])args[0]),
                        (uint)args[1]
                        );
                    break;
                case "getSecureIdCounter":
                    getSecureIdCounter(
                        Blockchain.GetAccount((byte[])args[0]),
                        (uint)args[1]
                        );
                    break;
                case "getSecureIdInfo":
                    getSecureIdInfo(Blockchain.GetAccount((byte[])args[0]));
                    break;
                case "getSecureIdDistribution":
                    getSecureIdDistribution(Blockchain.GetAccount((byte[])args[0]));
                    break;
                case "setDummy":
                    setDummy();
                    break;
            }
        }

        // call: SecureId_Factory(generateSecureId, _detailsJson);
        private static byte[] generateSecureId(string _detailsJson)
        {
            SecureId newId = new SecureId();
            byte[] sId = Blockchain.GetAccount(newId);

            personalSecureIds[Blockchain.GetAccount(this)].push(sId);
            secureIdPointers[sId] = personalSecureIds[msg.sender].length - 1;
            secureIdOwners[sId] = msg.sender;
            secureIdDetail[sId] = _detailsJson;
            lastSecureId[msg.sender] = sId;
        }

        //call: SecureId_Factory(generateSecureId, _secureId <spasi> _detailsJson);
        private static void updateSecureId(Account _secureId, string _detailsJson)
        {
            secureIdDetail[_secureId] = _detailsJson;

        }

        private static Account getLastSecureId(Account from)
        {
            return lastSecureId[from];
        }

        private static string getSecureIdInfoByOwner(Account _secureIdOwner, uint _i)
        {
            SecureId sId = SecureId(personalSecureIds[_secureIdOwner][_i]);
            return (secureIdDetail[sId]);
        }

        private static string getSecureIdDistributionByOwner(Account _secureIdOwner, uint _i)
        {
            SecureId sId = SecureId(personalSecureIds[_secureIdOwner][_i]);
            return (secureIdDistribution[sId]);
        }

        private static uint getSecureIdCounter(Account _secureIdOwner, uint _i)
        {
            SecureId sId = SecureId(personalSecureIds[_secureIdOwner][_i]);

            return (sId.counter());
        }

        private static string getSecureIdInfo(Account _secureId)
        {
            Account secureIdOwner = secureIdOwners[_secureId];
            uint contractIndex = secureIdPointers[_secureId];

            return getSecureIdInfoByOwner(secureIdOwner, contractIndex);
        }

        private static string getSecureIdDistribution(Account _secureId)
        {
            Account secureIdOwner = secureIdOwners[_secureId];
            uint contractIndex = secureIdPointers[_secureId];

            return getSecureIdDistributionByOwner(secureIdOwner, contractIndex);
        }

        private static void setDummy()
        {
            Account sId = new SecureId();
            personalSecureIds[msg.sender].push(sId);
            secureIdPointers[sId] = personalSecureIds[msg.sender].length - 1;
            secureIdOwners[sId] = msg.sender;
        }
    }
}
