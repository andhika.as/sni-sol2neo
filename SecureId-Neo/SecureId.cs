﻿using Neo.SmartContract.Framework;
using Neo.SmartContract.Framework.Services.Neo;
using Neo.VM;
//using Neo.SmartContract.Framework.Services.System;
//using System;
//using System.Numerics;
//using System.Collections;


namespace SecureId_Neo
{
    public class SecureId : SmartContract
    {
        private static string detailsJson;
        private static string distributionsJson;
        public static uint counter;

        //private static StorageContext detailsJson;
        //private static StorageContext distributionsJson;
        //public static StorageContext counter;

        // Main function
        // Create New: Contract1() 
        public static string Main(string operation, params object[] args)
        {
            string result = "Error: Invalid Operation";
            //string detailsJson = (string)args[0];
            //string distributionJson = (string)args[1];
            string input = (string)args[0];

            Runtime.Notify("Secureid, opt: ", operation);

            switch (operation) {
                case "":
                    counter = 0;
                    result = "Id Created";
                    break;
                case "getDetails":
                    result = getDetails();
                    break;
                case "getDistribution" :
                    result = getDistribution();
                    break;
                case "updateDetails":
                    // Need function test input
                    result = updateSecureId(input);
                    break;
                case "updateDistributions":
                    // Need function test input
                    result = updateDistributions(input);
                    break;
            }
            return result;
        }

        //private uint CounterId() {
        //    return counter;
        //}

        private static string addCounter() {
            counter++;
            return counter + "";
        }
       
        private static string getDetails() {
            return detailsJson;
        }

        private static string getDistribution() {
            return distributionsJson;
        }

        private static string updateSecureId(string _detailsJson) {
            detailsJson = _detailsJson;
            return "detail updated, with count: "+ addCounter();
        }

        private static string updateDistributions(string _distJson) {
            distributionsJson = _distJson;
            return "Detail Distribution updated.";
        }

    }
}
